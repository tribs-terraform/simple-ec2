# Déploiement d'une Instance EC2 AWS avec Terraform et GitLab CI/CD

Ce projet déploie une instance EC2 AWS en utilisant Terraform. Il peut être exécuté à partir d'une machine locale pour un déploiement distant ou via la pipeline GitLab.

## Prérequis

- Terraform
- Compte AWS avec les permissions appropriées
- (Optionnel) GitLab pour l'exécution via la pipeline

## Configuration des identifiants AWS

Avant de pouvoir exécuter Terraform, vous devez configurer vos identifiants AWS. Ces identifiants sont utilisés par Terraform pour créer et gérer les ressources AWS en votre nom.

1. Connectez-vous à la [console AWS IAM](https://console.aws.amazon.com/iam/).
2. Dans le volet de navigation, choisissez "Users", puis sélectionnez votre nom d'utilisateur IAM.
3. Dans la page de résumé de l'utilisateur, choisissez l'onglet "Security credentials".
4. Dans la section "Access keys", choisissez "Create access key".
5. Pour afficher la nouvelle clé d'accès, choisissez "Show". Vous ne pourrez pas afficher à nouveau le secret d'accès après cette boîte de dialogue est fermée. Vos identifiants ressembleront à ceci :
   - `AWS_ACCESS_KEY_ID` = AKIAIOSFODNN7EXAMPLE
   - `AWS_SECRET_ACCESS_KEY` = wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

6. Enregistrez ces clés dans un endroit sûr. Vous pouvez également télécharger les clés en choisissant "Download .csv file".

7. Configurez ces clés comme variables d'environnement dans GitLab :
   - Accédez à votre projet sur GitLab.
   - Dans le menu de gauche, naviguez vers "Settings" > "CI/CD".
   - Sous "Variables", ajoutez les deux variables : `AWS_ACCESS_KEY_ID` et `AWS_SECRET_ACCESS_KEY` avec leurs valeurs respectives.

## Déploiement

### Exécution Locale pour un Déploiement Distant

1. Modifiez les variables dans le fichier `variables.tf` selon vos besoins (région, type d'instance, nom de l'instance, etc.).
2. Exécutez `terraform init` pour initialiser le projet.
3. (Optionnel) Exécutez `terraform plan` pour visualiser les changements avant l'application.
4. Exécutez `terraform apply` pour déployer l'instance EC2 AWS.

### Exécution via GitLab pour un Déploiement Distant

1. Modifiez le fichier `.gitlab-ci.yml` selon vos besoins de pipeline.
2. Exécutez la pipeline GitLab pour déployer l'instance EC2 AWS.

## Fonctionnalités

- Déploiement d'une instance EC2 avec une AMI Debian 12.
- Récupération de l'adresse DNS publique de l'instance EC2 et stockage dans une variable locale.
- Ajout de données utilisateur au lancement de l'instance pour configurer le service SSH.
- Association d'un profil d'instance IAM au lancement de l'instance.
- Association d'un groupe de sécurité au lancement de l'instance pour autoriser le trafic SSH, HTTP et HTTPS.
- Génération automatique d'une paire de clés SSH pour l'instance et sauvegarde de la clé privée dans `~/.ssh/id_rsa`.
- Test automatique de la connexion SSH à l'instance EC2 après son déploiement.

## Outputs

Après le déploiement, vous pouvez obtenir l'adresse IP publique, l'ID de l'instance EC2 et l'adresse DNS publique à partir des sorties Terraform définies dans le fichier `output.tf`.

## Support

Pour toute question ou support, veuillez ouvrir une issue.

## Licence

Ce projet est sous licence MIT.
