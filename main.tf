# Génération d'une paire de clés SSH
resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Création d'une paire de clés AWS à partir de la clé publique générée
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = tls_private_key.example.public_key_openssh
}

# Stockage de la clé privée dans un fichier local
resource "local_file" "private_key" {
  sensitive_content = tls_private_key.example.private_key_pem
  filename          = "~/.ssh/id_rsa"
  file_permission   = "0600"
}

# Affichage de la clé publique pour débogage
resource "null_resource" "display_public_key" {
  provisioner "local-exec" {
    command = "echo '${tls_private_key.example.public_key_openssh}'"
  }
  triggers = {
    always_run = "${timestamp()}"
  }
}

# Affichage de la clé privée pour débogage
resource "null_resource" "display_private_key" {
  provisioner "local-exec" {
    command = "echo '${tls_private_key.example.private_key_pem}'"
  }
  triggers = {
    always_run = "${timestamp()}"
  }
}

# Déploiement d'une instance EC2 avec la clé publique
resource "aws_instance" "my_vm" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = aws_key_pair.deployer.key_name

  tags = {
    Name        = var.name_tag
    Description = var.description_tag
  }

  # Configuration de l'instance pour accepter les connexions SSH avec la clé RSA
  user_data = <<-EOF
              #!/bin/bash
              echo "PubkeyAcceptedAlgorithms +ssh-rsa" >> /etc/ssh/sshd_config
              systemctl restart sshd
              EOF

  iam_instance_profile = aws_iam_instance_profile.simple_ec2.name

  vpc_security_group_ids = [aws_security_group.simple_ec2.id]

  # Tentative de connexion SSH à l'instance pour vérification
  provisioner "local-exec" {
    command = "echo '${tls_private_key.example.private_key_pem}' > ~/.ssh/id_rsa && chmod 600 ~/.ssh/id_rsa && ssh-keyscan -H ${aws_instance.my_vm.public_dns} >> ~/.ssh/known_hosts && ssh-add ~/.ssh/id_rsa && ssh -vvv -i ~/.ssh/id_rsa ${local.ssh_user}@${aws_instance.my_vm.public_dns} || true"
  }
}

# Configuration d'un profil IAM pour l'instance EC2
resource "aws_iam_instance_profile" "simple_ec2" {
  name = "simple_ec2-profile"
  role = aws_iam_role.simple_ec2.name
}

# Création d'un rôle IAM pour l'instance EC2
resource "aws_iam_role" "simple_ec2" {
  name = "simple_ec2-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# Configuration du groupe de sécurité pour l'instance EC2
resource "aws_security_group" "simple_ec2" {
  name        = "simple_ec2"
  description = "Simple ec2 security group"

  # Règles d'entrée pour SSH, HTTP et HTTPS
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Variables locales pour le script
locals {
  ssh_user = "admin"
}
