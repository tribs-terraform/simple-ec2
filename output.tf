# ID de l'instance EC2 déployée
output "instance_id" {
  value = aws_instance.my_vm.id
}

# IP publique de l'instance EC2 déployée
output "instance_public_ip" {
  value = aws_instance.my_vm.public_ip
}

# DNS public de l'instance EC2 déployée
output "instance_public_dns" {
  value = aws_instance.my_vm.public_dns
}
