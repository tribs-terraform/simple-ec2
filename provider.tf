# Configuration des providers nécessaires et de leurs versions
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.12.0"
    }
  }

  # Configuration du backend pour stocker l'état de Terraform sur GitLab
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/48463971/terraform/state/default"
    lock_address   = "https://gitlab.com/api/v4/projects/48463971/terraform/state/default/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/48463971/terraform/state/default/lock"
  }
}

# Configuration du provider AWS avec la région spécifiée
provider "aws" {
  region = var.region
}
