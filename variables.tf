# Définition de l'AMI à utiliser pour l'instance EC2
variable "ami" {
  type        = string
  description = "Debian 12 AMI ID in us-east-1 Region"
  default     = "ami-06db4d78cb1d3bbf9" // AWS AMI
}

# Type d'instance EC2 à déployer
variable "instance_type" {
  type        = string
  description = "The type of instance to start"
  default     = "t2.micro"
}

# Tag "Name" pour l'instance EC2
variable "name_tag" {
  type        = string
  description = "Debian 12 instance"
  default     = "Generic Debian 12 instance for wordpress deployment"
}

# Tag "Description" pour l'instance EC2
variable "description_tag" {
  type        = string
  description = "The Description tag for the instance"
  default     = "My EC2 instance"
}

# Région AWS où l'infrastructure sera déployée
variable "region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}
